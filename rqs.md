RQ1: Structure is such that it trains
    - Converges
        -> Otherwise it's not learning, so it's not generating meaningfully
        1.1 Does the training loss go down?
    - Maintains diversity
        -> Otherwise it learns to generate only a few things. Both the training process and the actualy program behaviour exploration both suffer
        1.2 Do we keep getting diverse outputs?
    - Latent space is useful
        -> Otherwise the locality in the latent space is meaningless
        1.3 Does the latent space look like a manifold? (related to RQ3: does it make a difference where we prime from?)

RA1:
    - Use moderate optimisers
        -> Slower, but given the noise and the moving target dataset, it's the only stable way
    
    - Too much noise
        -> Won't converge
        -> Becomes unstable
        -> Latent space is useless

    - Too little noise
        -> Datapoints get mapped very close to each other in the latent space. This weakens their signal for the GNN.

    - Too weak, i.e. too few cells or layers
        -> Won't converge
        -> Will keep producing random noise
    - Too strong, i.e. too many cells or layers
        -> Will memorise things
        -> Will end up producing very non-diverse outputs

    - Don't make the VAE too powerful nor too weak
        -> This will affect the locality in the latent space, analysed in RQ3.
    

RQ2: The outputs appear syntactically interesting
    -> We might have trained and found diversity, but it was nonsensical.
    2. Do the produced strings share syntactic features with AFL generated baselines?
        -> The reason we compare against AFL is that we share the representation.
    
RA2:
    - Yea, the generated things are clearly not just random shite


RQ3: The location in the latent space is meaningful
    
    - We ought to be able to generate various program inputs from noise
        -> Otherwise changing the noise input would not make any difference
        3.1 Do we get varied outputs by generating from different points?
    
    - We want the generated things to be similar if generated from adjacent points and dissimilar if generated from distant ones.
        -> Otherwise the positions in the latent space would not be meaningfully ordered.
        3.2 Are the strings similar when generated from adjacent inputs vs. from distant ones?

    - Can we target a specific execution?
        -> Otherwise we did not learn to approximate the program behaviour.
        3.3 Does the encoding of the execution trace of the generated string match that of the input?

    
RA3:
    3.1 Yes, we get varied outputs.
    3.2 Yes
    3.3 Hmm, seems not. It's better than random, but not as good as we had hoped. Improving this is one possible next step.
    

RQ4: Can a trained DCGAF produce diverse paths and trigger crashes?
    -> Otherwise it might not have the potential for being a fuzzer

RA4: 
    - Yea, we discover plenty of paths